//
//  Maze.cpp
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#include "Maze.h"
#include "Menu.h"
#include "Spacer.h"
#include <iostream>
#include <fstream>
using namespace std;

// Maze output function
void Maze::mazeOutput()
{   
    curs_set(0);
    if(!generated)                                  // Check if the map has not been generated yet
    {
        generateMap();
        generated = true;
    }
    move(2,1);
//    cout << "Level: " << level <<endl;
    printw("Level: %i\n", level);
    refresh();
    for(int i = 0; i<yRange; ++i)
    {
        for(int x =0; x<xRange; ++x)
        {
//            cout << Maze::map[i][x];                // Prints each element of the map array
            switch (Maze::map[i][x])
        {
            case 'u':
                attron(COLOR_PAIR(2) | A_BOLD);
                break;
            case 't':
            case 'c':
                attron(COLOR_PAIR(1));
                break;
            case 'b':
                attron(COLOR_PAIR(4));
                break;
        }
            printw("%c", Maze::map[i][x]);
            attroff(COLOR_PAIR(1) | A_BOLD);
            refresh();
        }
//        cout << endl;
        printw("\n");
        refresh();
    }
    curs_set(1);
    Maze::mazeInput(Maze::userPtrX, Maze::userPtrY);// Call the maze input function with the user coordinates
}

// Maze input function
void Maze::mazeInput(int* userX, int* userY)        // Could just directly reference the original pointers
{
    Menu menuObj;
    Spacer spacerObj;
    
    int userInput;
//    cin >> userInput;                             // Takes the user input
    userInput = getch();
    
    int directionY{*userY};                         // Creates the new user position variable Y and sets it to
                                                    //the current position
    int directionX{*userX};                         // Creates the new user position variable X and sets it to
                                                    //the current position
    
    switch(userInput)                               // Check userinput for direction and change position accordingly
    {                                               //in relation to the original position
        case 'u':
        case KEY_UP:
            directionY = *userY - 1;
            Maze::checkClear(directionX, directionY); // Having this line at the bottom of the function was causing problems with quitting. I think it would get into layers of functions so would go through the menu a couple of times before the program finally ended. Should be fixed
            break;
        case 'd':                                     // Down
        case KEY_DOWN:
            directionY = *userY + 1;
            Maze::checkClear(directionX, directionY);       // Feed position to checkClear to alter map array
            break;
        case 'l':                                     // Left
        case KEY_LEFT:
            directionX = *userX -1;
            Maze::checkClear(directionX, directionY);
            break;
        case 'r':                                     // Right
        case KEY_RIGHT:
            directionX = *userX + 1;
            Maze::checkClear(directionX, directionY);
            break;
        case 'q':                                     // Quit
            spacerObj.spacer();
            menuObj.mainCopy();                             // Returns to the menu
            break;
        case 's':                                     // Save
            spacerObj.spacer();
            Maze::saveToFile();
            Maze::mazeOutput();
            break;
        case 'p':                                     // Place object
            directionY = *userY - 1;
//            spacerObj.spacer();
            if(map[directionY][directionX] == ' ')          // Check if proposed place is clear
            {
                map[directionY][directionX] = objectChar;   // Change proposed place to held object character
                objectChar = ' ';                           // Held object is "emptied"
                Maze::mazeOutput();
                break;
            }

        default:
//            cout << "Try again\n";                  // if input is not understood
            printw("Try again\n");
            refresh();
            Maze::mazeOutput();
    }
    
}

// Check if the target space is clear
void Maze::checkClear(int directionX, int directionY)
{
    char x;                                         // Throwaway char to pause program until user input
    
    if (Maze::map[directionY][directionX] == ' ')   // Checks that the destination space is clear
    {
        Maze::map[userCoor[0]][userCoor[1]] = ' ';  // Change the occupied space to blank
        Maze::map[directionY][directionX] = 'u';    // Change the destination space to u
        userCoor[0] = directionY;                   // Set the Y co-ordinate record
        userCoor[1] = directionX;                   // Set the X co-ordinate record
//        spacerObj.spacer();
        didTheyFinish();
//        clear();
        Maze::mazeOutput();                         // Refresh the map
    }
    
    else if (map[directionY][directionX] == '#')
    {
//        spacerObj.spacer();
//        cout << "This is a wall\n" << "Continue?\n";
        printw("This is a wall\nContinue?\n");
        refresh();
//        cin >> x;
        getch();
        clear();
        Maze::mazeOutput();
    }
    
    else if (map[directionY][directionX] == 'c')
    {
//        spacerObj.spacer();
//        cout<< "This is a chair.\nYou can't sit down. You can pick it up though.\n[m]ove or continue [any key]?\n";
        printw("This is a chair.\nYou can't sit down. You can pick it up though.\n[m]ove or continue [any key]?\n");
        refresh();
//        cin >> x;
        x = getch();
        if(x == 'm')                                        // Move the chair
        {
            if(objectChar == ' ')                           // Check player is not already carrying something
            {
                objectChar = map[directionY][directionX];   // Set the "inventory" to c
                map[directionY][directionX] = ' ';          // Empty the map element
            }
            else
            {
//                spacerObj.spacer();
                
//                cout << "You're not Superman. You can carry ONE chair at a time\n\tContinue?";
                printw("\n\nYou're not Superman. You can carry ONE chair at a time\n\tContinue?");
                refresh();
//                cin >> x;
                getch();
                
            }

        }
//        spacerObj.spacer();
        clear();
        Maze::mazeOutput();
    }
    
    else if (map[directionY][directionX] == 'b')
    {
//        spacerObj.spacer();
//        cout << "Bookshelf probably?\n\tContinue?\n";
        printw("Bookshelf?\n Continue?\n");
        refresh();
//        cin >> x;
        getch();
        clear();
        Maze::mazeOutput();
    }
    
    else if (map[directionY][directionX] == 't')
    {
//        spacerObj.spacer();
//        cout << "Well this is a table. Why would anyone need so many and shouldn't the chairs be collected around it I hear you ask.\nWell that's random generation for you.\nGet over it and keep going? (psst you don't have a choice)\n";
        printw("Well this is a table. Why would anyone need so many and shouldn't the chairs be collected around it I hear you ask.\nWell that's random generation for you.\nGet over it and keep going? (psst you don't really have a choice)\n");
        refresh();
//        cin >> x;
        getch();
        clear();
        Maze::mazeOutput();
    }
        
}

// Are they in the end square?
void Maze::didTheyFinish()
{
//    Spacer spacerObj;
//    char x;                                         // Throwaway char to carry on
    
    if(userCoor[0] == 1 && userCoor[1] == xRange-2) // Check they are in the finish square
    {
        if(objectChar == ' ')                       // Check if they are carrying a chair
        {
//            spacerObj.spacer();
//            cout << "YAY! Next level:\n";
            clear();
            mvprintw(0,0,"YAY! Next level:\n");
            refresh();
            ++level;
            generateMap();
            userCoor[0] = 6;
            userCoor[1] = 4;
        }
        else
        {
//            spacerObj.spacer();
//            cout << "Sir, Please put down the chair.\n\tContinue?";
            printw("Sir, Please put down the chair.\n\tContinue?");
//            cin >> x;
            getch();
            clear();
        }
        
    }
}

// Save to file function
void Maze::saveToFile()                                                     // Moved here to easily access the array
{
    endwin();
    if(!saveFileEntered)                                                    // Returns true if there is no saved directory
    {
        cout << "\n\nEnter a directory to the save file. Then press Enter.\n";
//        printw("Enter a directory to the save file\n");
        cin >> saveFileDirectory;
//        getstr(saveFileDirectory);
        saveFileEntered = true;
    }
    else
    {
        char answer;
        cout << "Would you like to retype the save file directory? (y/n)\n";// In case the directory was typed wrong
        cin >> answer;
        Spacer spacerObj;
        spacerObj.spacer();
        if(answer == 'y')
        {
            cout << "Enter a directory to the save file\n";
            cin >> saveFileDirectory;
        }
    }
    ofstream saveFile(saveFileDirectory);                                   // Print the array elements into the save file
    saveFile << level;
    for(int i = 0; i<this->yRange; ++i)
    {
        for(int x =0; x<this->xRange; ++x)
        {
            saveFile << map[i][x];
        }
    }
    saveFile.close();
    refresh();
    clear();
}

// Load from file function
void Maze::loadFromFile()
{

    endwin();
    cout << "\n\nEnter the save file directory path\n";
    cin >> saveFileDirectory;                               // Universal file directory so don't have to re enter to save
    saveFileEntered = true;
    generated = true;                                       // So that it doesn't generate a new map after loading the saved one

    ifstream saveFile(saveFileDirectory);
    
    char element;
    saveFile >> level;
    for(int i = 0; i<yRange; ++i)
    {
        for(int x =0; x<xRange; ++x)
        {
            saveFile.get(element);                      // Gets every single character. Including the spaces
            Maze::map[i][x] = element;
            if(element == 'u')                          // Checks for the 'u' character so it can correct the player position array
            {
                *userPtrY = i;
                *userPtrX = x;
            }
        }
    }
    saveFile.close();                                   // Close the text tile
    refresh();
    clear();
    Maze::mazeOutput();
}

//map generation function
void Maze::generateMap()
{
    int objectKey;                                                  // contains the randomly generated number
    char object;                                                    // contains the object char
    
    srand(static_cast<unsigned int>(time(0)));                                                 // random seed
    
    for(int i = 0; i<yRange; ++i)                                   // loop
    {
        for(int x =0; x<xRange; ++x)
        {
            if(i == 0 || i == yRange-1 || x == 0 || x == xRange-1)  // put # around the edges of the map
            {
                Maze::map[i][x] = '#';
            }
            else if(i == 6 && x == 4)                               // puts the player in the same spot every time
            {
                Maze::map[i][x] = 'u';
            }
            else if(i == 1 && x == xRange-2)                        // make sure the top right corner is free
            {
                Maze::map[i][x] = ' ';
            }
            else                                                    // converts the random number to an object char
            {
                objectKey = rand()%16;
                
                switch(objectKey)
                {
                    case 0:
                        object = '#';
                        break;
                    case 1:
                        object = 'c';
                        break;
                    case 2:
                        object = 'b';
                        break;
                    case 3:
                        object = 't';
                        break;
                    default:
                        object = ' ';
                        break;
                }
                Maze::map[i][x] = object;                           // places the object char in the map array
            }
        }
    }
}
