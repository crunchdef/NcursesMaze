//
//  Maze.h
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#ifndef __Maze__Maze__
#define __Maze__Maze__

#include <stdio.h>
#include <iostream>
#include <ncurses.h>
using namespace std;

class Maze
{
private:
    int userCoor[2]{6,4};        // Keeps track of the users position starting position based on the below array
    int *userPtrY{&userCoor[0]}; // Provides a pointer to the y coordinate of the userCoor array
    int *userPtrX{&userCoor[1]}; // Provides a pointer to the x coordinate of the userCoor array
    char objectChar{' '};        // Stores the held object character
    
    int level{1};                   // Level number
    
    bool generated{false};       // Check if map has been generated
    bool saveFileEntered{false}; // Check if a save file directory path has already been inputted
    string saveFileDirectory;
    
    int yRange{8};               // SHOULD MATCH THE MAP ARRAY!
    int xRange{16};              // SHOULD MATCH THE MAP ARRAY!
    char map[8][16]{
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#',' ',' ',' ','c',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#',' ',' ',' ','u',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#'},
        {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}};
public:
    void mazeOutput();                               // Prints out the map on screen
    void mazeInput(int* userX, int* userY);          // Takes in the direction commands
    void checkClear(int directionX, int directionY); // Check the area being moved into is clear and alter the array
    void saveToFile();
    void loadFromFile();
    void generateMap();
    void didTheyFinish();                            // Check if they are in the finish square and not carrying a chair
};

#endif /* defined(__Maze__Maze__) */
