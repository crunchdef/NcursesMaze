//
//  Menu.cpp
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#include "Menu.h"
#include "Spacer.h"
#include "Maze.h"
#include <iostream>

using namespace std;

// menu output function
void Menu::menuOutput()
{
//    cout << "\t\t" << "----[p]lay----\n";
//    cout << "\t  " << "----[i]nstructions---\n";
//    cout << "\t   " << "----[l]oad File----\n";
//    cout << "\t\t" << "----[q]uit----\n";
    
    mvprintw(2,11,"-----[p]lay-----");
    mvprintw(3,10,"--[i]nstructions--");
    mvprintw(4,9,"----[l]oad File----");
    mvprintw(5,11,"-----[q]uit-----");
}

//menu input function
int Menu::menuInput()
{
    char userInput;
//    cin >> userInput;
    raw();
    noecho();
    userInput = getch();
    
    
    string quitMessage{"Goodbye!\n"};
    string unrecognisedInput{"I'm sorry I don't understand that. Please retry a key\n"};
    
    Spacer systemObj;
    Maze mazeObj;
    
    switch(userInput)                           // compares the input against each menu item
    {
        case 'p':
            clear();
            mazeObj.mazeOutput();
            break;
        case 'i':
//            systemObj.spacer();
            move(0,0);
            instructions();
            break;
        case 'l':
            systemObj.spacer();
            mazeObj.loadFromFile();
            break;
        case 'q':
//            systemObj.spacer();
//            cout << quitMessage;
            clear();
            addstr("Goodbye!\n");
            halfdelay(8);
            getch();
            endwin();
            return 0;
        default:
//            systemObj.spacer();
//            cout << unrecognisedInput;
            printw("\n\nI'm sorry I don't understand that. Please retry a key\n");
            mainCopy();
    }
    return 0;
}

//instructions menu function
void Menu::instructions()
{

//    cout << "\t\t Instructions\n";
//    cout << "Move around with [u]p, [d]own, [l]eft and [r]ight.\nPlace carried chairs with [p].\nSave with [s].\n" <<
//        "You're trying to get to the top right corner\nReturn to the menu with any key\n";
//    cin >> n;
    printw("\t\t Instructions\nMove around with [u]p, [d]own, [l]eft and [r]ight or with the arrow keys thanks to the magic of ncurses!.\nPlace carried chairs with [p].\nSave with [s].\nYou're trying to get to the top right corner\nReturn to the menu with any key\n");
    getch();
    
    clear();
    mainCopy();
}

void Menu::mainCopy() // to emulate the main in order to be able to call it in order to get back to the main menu
{
    Menu::menuOutput();
    Menu::menuInput();
}