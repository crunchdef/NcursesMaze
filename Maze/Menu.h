//
//  Menu.h
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#ifndef __Maze__Menu__
#define __Maze__Menu__

#include <stdio.h>
#include <ncurses.h>

class Menu
{
public:
    void menuOutput();
    int menuInput();
    void instructions();
    void mainCopy();
};

#endif /* defined(__Maze__Menu__) */
