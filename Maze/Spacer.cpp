//
//  Spacer.cpp
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#include "Spacer.h"
#include <iostream>
using namespace std;

void Spacer::spacer()
{
    cout << "\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f\f";
}