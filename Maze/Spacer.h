//
//  Spacer.h
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#ifndef __Maze__Spacer__
#define __Maze__Spacer__

#include <stdio.h>

class Spacer
{
public:
    void spacer(); //spacer function
};

#endif /* defined(__Maze__Spacer__) */
