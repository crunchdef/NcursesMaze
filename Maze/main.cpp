//
//  main.cpp
//  Maze
//
//  Created by Matthew Hitchman on 15/01/2016.
//  Copyright (c) 2016 Matthew Hitchman. All rights reserved.
//

#include <iostream>
#include "Maze.h"
#include "Spacer.h"
#include "Menu.h"
#include <ncurses.h>

using namespace std;

int main() {
    initscr();
    keypad(stdscr, TRUE);
    start_color();
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_MAGENTA, COLOR_BLACK);
    
    Menu menuObj;
//    cout << "\t   Press the key in []\nThen press enter to input commands\n" << endl;
    printw("\t   Press the key in []\n");
    refresh();
    menuObj.mainCopy();
    
    return 0;
}




